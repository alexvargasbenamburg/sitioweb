---
title: "GIT"
date: 2018-09-04T12:21:26-06:00
draft: true
tags: ["git","backup","versiones", "gitlab","github"]
---

## Por qué?
En lo personal lo uso como medida de respaldos, trabajo compartido sobre el mismo proyecto. Por la forma en que lleva el registro de cambios el los achivos se puede ver quien y cuando escribio cada parte del codigo.

## Que es?
GIT es un software de control de versiones desarrollado por [Linus Torvalds](https://es.wikipedia.org/wiki/Linus_Torvalds), este esta pensando en la eficiencia y la confiabilidad del mantenimiento de versiones de aplicaciones cuando éstas tienen un gran número de archivos de código fuente. Su propósito es llevar registro de los cambios en archivos de computadora y coordinar el trabajo que varias personas realizan sobre archivos compartidos.

## Instalacion

#### Linux
Centos: ``` sudo yum install git ```  
Debian & Ubuntu: ``` sudo apt install git ```  

#### Windows
Usa este instalador [http://msysgit.github.com/](http://msysgit.github.com/)  
O bien este otro [https://git-scm.com/downloads](https://git-scm.com/downloads)  

#### Mac OS
Puedes usar este instalador [http://sourceforge.net/projects/git-osx-installer/](http://sourceforge.net/projects/git-osx-installer/)  
Si prefires puedes instalarlo atraves de [MacPorts](http://www.macports.org) ``` sudo port install git-core +svn +doc +bash_completion +gitweb ```  
O bien atraves de [Homebrew](http://brew.sh/) ``` brew install git ```  

## Uso
El uso es muy sencillo, aunque lleva tiempo acostumbrarse a hacer `commit` despues de cada cambio realizado.

### Iniciar