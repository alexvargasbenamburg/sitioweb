---
title: "HUGO + GitLab Pages"
date: 2018-09-04T12:21:26-06:00
draft: false
tags: ["hugo","html","git","gitlab"]
---

## Por qué?
En cierto momento tuve problemas económicos, y para poder seguir adelante decidí montar mis sitios webs( estaticos ) en Gitlab Pages para reducir los servidores que tenia en linea, Gitlab Pages es un servicio gratuito y fácil de usar, básicamente con tener tu sitio en un repositorio git y hacer push actualizas tu página, puedes encontrar mas informacion aca: [GITLAB PAGES](https://about.gitlab.com/features/pages/).

## Hugo
Este sitio esta montado sobre un framework para paginas estaticas muy facil de usar [HUGO](https://gohugo.io/), en la pagina de ellos puedes encontrar muy buena documentación, y por la internet puedes encontrar muchos tutoriales.

## Base
Puedes basarte en este [ejemplo](https://gitlab.com/pages/hugo) aunque lo importante es el archivo **.gitlab-ci.yml** ubicado en la raíz del repositorio.

### Archivos .gitlab-ci.yml
Los archivos **.gitlab-ci.yml** existen para decirle a Gitlab como debe de construirse, probarse, desplegarse y monitorizarse tu repositorio, si quieres mas informacion puedes verla aca: [GITLAB CI/CD](https://about.gitlab.com/features/gitlab-ci-cd/), con esta herramienta es facil tener una idea de como esta tu repositorio, por ejemplo con una etiqueta como la que está debajo de este párrafo que indica que el repositorio pasó la fase test, también puedes generar compilaciones, paquetes comprimidos, etc...
[![pipeline status](https://gitlab.com/alexvargasbenamburg/sitioweb/badges/master/pipeline.svg)](https://gitlab.com/alexvargasbenamburg/sitioweb/commits/master)

En este caso lo usaremos para ejecutar hugo y generar los HTMLs luego servirlos en GITLAB PAGES, dejo el contenido del archivo **.gitlab-ci.yml**

```
image: registry.gitlab.com/pages/hugo:latest

variables:
 GIT_SUBMODULE_STRATEGY: recursive

test:
 script:
 - hugo
 except:
 - master

pages:
 script:
 - hugo
 artifacts:
   paths:
   - public
 only:
 - master
```
### Como?
Si agregas este archivo a la raíz de tu repositorio y lo subes al servidor de Gitlab este comenzará a ejecutar en un Pipeline la construcción del sitio, en caso de error te envía un correo electrónico. Ahora bien con solo eso ya tenemos nuestro sitio construido y desplegado para acceder a él vamos a **https://namespace.gitlab.io/nombredelrepositorio**, **namespace** hace referencia a el nombre del grupo o el nombre de usuario donde esta alojado el repositorio, por ejemplo mi sitio está en **https://gitlab.com/alexvargasbenamburg/sitioweb** donde **alexvargasbenamburg** es el **namespace** y **sitioweb** es el nombre del repositorio, asi quedaria [https://alexvargasbenamburg.gitlab.io/sitioweb](https://alexvargasbenamburg.gitlab.io/sitioweb).

### Problemas?
No te asustes, puede que tu sitio no se despliega de forma correcta, **HUGO** usa ruta absolutas por ende como nuestro sitio web no esta en la raiz, los archivos y rutas estaran mal, esto lo podemos solucionar con el parametro `relativeURLs = true` en el archivo [config.toml](https://gitlab.com/alexvargasbenamburg/sitioweb/blob/master/config.toml)

### Dominio propio
Bueno es notable que para acceder a este sitio tambien puedes hacerlo a travez de [https://vbalex.com](https://vbalex.com), ambos sitios apuntan a los mismos archivos, para hacer esto debes ir a Configuracion->Pages en el repositorio en Gitlab.
![config-pages](../../imagenes/tutoriales/gitlabpages/dp1.png?classes=border,shadow&width=50%)

Luego vas a crear un nuevo dominio y llegas a esta pestana:
![newdomain](../../imagenes/tutoriales/gitlabpages/dp2.png?classes=border,shadow&width=50%)

Aca debes de poner el nombre de tu dominio y copiar el contenido de tu certificado unido al certificado de intermedio(en caso de existir) y llave privada. Ahora bien en caso de que no tengas un certificado podemos hacer uso de certificados de [https://letsencrypt.org/](https://letsencrypt.org/), para esto requerimos una máquina con sistema operativo linux, en caso de no tener una máquina disponible podemos usar la plataforma de [Cloud9](https://c9.io/) para general los certificados, esto será algo en lo que no voy a entrar. Dentro de una terminal ejecutaremos los siguientes comandos.
**Importante:** Cambiar mi dominio por el tuyo. es decir que si tu dominio es **dominio.abc** la instrucción sería:
**./letsencrypt-auto certonly -a manual --preferred-challenges=dns -d dominio.abc**
``` bash
git clone https://github.com/letsencrypt/letsencrypt
cd letsencrypt
./letsencrypt-auto certonly -a manual --preferred-challenges=dns -d midominio.com
```
Pregunta por tu correo electrónico, y algunas otras cosas, responde a conciencia :P, en un punto te va a pedir que registres un DNS TXT, esto se realiza en tu servidor de DNS, en mi caso uso [GoDaddy](https://godaddy.com/) adjunto las imagenes de los registros, usar los datos dados por letsencrypt. Importante remover el **.midominio.com** del nombre de host, por ejemplo letsencrypt me dice que agregue **_acme-challenge.vbalex.com** en GoDaddy debo de agregar el host **_acme-challenge** y el contenido que dicte el programa. Hacer esto antes de presionar ENTER.
![newdomain](../../imagenes/tutoriales/gitlabpages/gd0.png?classes=border,shadow&width=50%)

Al terminar te desplegara mucha información, entre ella la ruta de los certificados, el mi caso los certificados quedaron en **/etc/letsencrypt/live/vbalex.com/fullchain.pem** y **/etc/letsencrypt/live/vbalex.com/privkey.pem**, puedes usar el comando cat para desplegar el contenido de estos archivos asi: `cat /etc/letsencrypt/live/vbalex.com/fullchain.pem` copias el contenido y lo pegas el Gitlab en el apartado de Certificado(PEM) y luego `cat /etc/letsencrypt/live/vbalex.com/privkey.pem` copias el contenido y lo pegas en Gitlab en el apartado de Key(PEM) le das crear nuevo dominio.
Ahora debemos verificar el sitio en gitlab(lo mismo que hicimos para verificar el dominio en letsencrypt) en mi caso me dice que debo agregar **_gitlab-pages-verification-code.vbalex.com TXT gitlab-pages-verification-code=093ecc78d75f6572ef809b57fd3e3e2f** en este caso en GoDaddy debo agregar **_gitlab-pages-verification-code** como host y con el valor **gitlab-pages-verification-code=093ecc78d75f6572ef809b57fd3e3e2f**
![newdomain](../../imagenes/tutoriales/gitlabpages/gd1.png?classes=border,shadow&width=50%)

Para terminar de configurar solo debes de agregar un registro A a los DNS, con host **@** y dirección **52.167.214.135**. Importante saber que esta dirección puede cambiar es la dirección IP del dominio **gitlab.io**, lastimosamente GoDaddy en este momento no soporta registros CNAME sobre el host **@**.

### Créditos
https://gitlab.com/pages/hugo
https://about.gitlab.com/features/pages/
https://docs.gitlab.com/ee/user/project/pages/
https://letsencrypt.org/
https://about.gitlab.com/2016/04/11/tutorial-securing-your-gitlab-pages-with-tls-and-letsencrypt/

