---
title: "Generar Gerber"
date: 2018-09-05T17:16:14-06:00
draft: false
weight: 200
---
Los archivos en formato *GERBER* son imagenes vectoriales de 2 dimenciones, es el formato estandar para PCBs
Para este proyecto generare 2 archivo *GERBER*, uno del cobre de atras de la placa y de los huecos de los componentes.

### Monta tu proyecto en KiCad
Antes de comenzar, claramente debes de tener tu proyecto en KiCad, si no conoces la herramienta te dejo esta [lista de videos tutoriales](https://www.youtube.com/watch?v=iAISdXLVa3A) no son mios pero pueden ayudarte mucho.

### Generar Gerber del cobre
Para generar los archivos GERBER debes de estar en la herramienta Pcbnew con tu proyecto abierto, y das click en el icono de plot(impresora grande)
![plot](../../../imagenes/tutoriales/kicadtogrbl/t0.png?width=40%)

Te mostrara esta pestania, primero escoje la carpeta destino para los archivos**(1)**; normalmente creo una carpeta llamada *GERBER* dentro del mismo proyecto KiCad, luego selecionla las capas que vas a usar**(2)**, en mi caso serian *F. Cu* y *Edge.Cuts*, verificamos que los demas valores estan como en la imagen, y precionamos el boton de *PLOT* **(3)**, ahora precionamos el boton de *Generate Drill Files* **(4)** para generar el *GERBER* de los huecos.
**Nota:** En mi caso use la capa *F. Cu* (color rojo) para las pistas, si usaste la capa *B. Cu* debes de usarla en lugar de *F. Cu* cuando generamos los *GERBER*
**Nota:** Estamos trabajando con placas de un solo lado, por eso solo selecionamos una de las capas de cobre * Cu*.
![plot](../../../imagenes/tutoriales/kicadtogrbl/t1.png?width=40%)

Nos abrira una nueva pestana, al igual que la pestana anterior se escoje la carpeta de destino **(1)**, luego verificamos que las opciones esten como en la imagen, y precionamos *Generate Drill File* **(2)**, con esto deberiamos de tener 3 archivos en la carpeta que selecionamos como destino, dos con extencion **gbr** y otro **drl**. 
![plot](../../../imagenes/tutoriales/kicadtogrbl/t2.png?width=40%)

Con esto hemos terminado de generar los archivos GERBER que nesesitamos.

El siguiente paso es [Generar el G-code](../gerberagcode) a partir de los archivos *GERBER*.