---
title: "PCB a CNC"
date: 2018-09-05T16:16:16-06:00
draft: false
tags: [cnc,grbl]
---

## Por qué?
Hace un tiempo adquiri una [mini CNC](http://www.crcibernetica.com/mini-cnc-router-engraver-kit/) para trabajar en con PCBs, debo admitir que tube bastante que probar y realizar prueba tras pueba para tener un resultado aceptable.

## PCB 
PCB son la siglas printed circuit board, es la placa de color verde(normalmente) donde estan los circuitos electronicos puestos.

|                                                        |                                                        |
| :----------------------------------------------------: | :----------------------------------------------------: |
| ![PCB](../../imagenes/tutoriales/kicadtogrbl/pcb0.png) | ![PCB](../../imagenes/tutoriales/kicadtogrbl/pcb1.png) |

## KiCad
[KiCad](http://kicad-pcb.org/) es un software para generacion de esquematicos, disenio de PCB, es un software opensource con buena documentacion y muchos aportes de parde de la comunidad.

## GRBL
[GRBL](https://github.com/grbl/grbl) es el firmware con el cual vienen cargadas la mayoria de CNCs de bajo costo, es relativamente facil de medificar, configurar y usar, se comunica por [G-code](https://es.wikipedia.org/wiki/G-code), y normalmente para operarlo utilizo un software llamado **bCNC**.
**GRBL** tiene la ventaja de poderser ejecutar dentro de un [Arduino UNO](https://store.arduino.cc/arduino-uno-rev3), por ello se a vuelto popular entra las CNCs de bajo costo, ademas tiene buen soporte para distintas configuraciones de maquina.

## bCNC 
[bCNC](https://github.com/vlachoudis/bCNC) es un software opensource, para manejo de maquinas con el firmware GRBL, tiene la utilidad de crear un mapa de auto-nivelado muy util para procesos que requieren presicion, ademas puedes cargar archivos svg y dxf sin nesesidad de convertirlos a GCODE.

## pcb2gcode
Tanto [pcb2gcode](https://github.com/pcb2gcode/pcb2gcode) como [pcb2gcodeGUI](https://github.com/pcb2gcode/pcb2gcodeGUI) son herramientas opensource creadas para transformar los archivos gerber en gcode, en lo personal uso **pcb2gcodeGUI** por un tema de facilidad, pero si eres de los que detesta las ventanas puedes usar **pcb2gcode**.
*NOTA:* pcb2gcodeGUI utiliza pcb2gcode por debajo.


El primer paso es [Generar los archivos GERBER](./generargerber).