---
title: "Gerber a G-code"
date: 2018-09-05T20:03:33-06:00
draft: false
weight: 201
---
Para generar los G-code requieres la herramienta *pcb2gcode*, en mi caso usare *pcb2gcodeGUI* para facilitar las cosas.
![p2g](../../../imagenes/tutoriales/kicadtogrbl/g1.png?width=40%)

El programa tiene varios parametros, aun asi no suelo usar ninguno, debido a que modifico el *G-code* en **bCNC**, se me es mas facil de hacer las cosas asi.
Basicamente lo que hacemos es selecionar nuestros archivos, primero el Front **(1)** buscamos el *F.Cu*, para el Outline **(2)** buscamos el *Edge.Cuts*, para el Drill **(3)** buscamos el archivo de extencion *.drl*, puedes ver a la derecha una imagen con tu placa, para ultimo debes escojer el folder de destino para los archivos *G-code*; yo normalmente creo una carpeta llamada *GCODE* dentro del proyecto de *KiCad*.
![p2g1](../../../imagenes/tutoriales/kicadtogrbl/g2.png?width=40%)

