+++
title = "Inicio"
chapter = false
weight = 5
pre = "<b> </b>"
+++

## Alex Vargas Benamburg <sub>[+506 7097 8755](tel:+50670978755) - [yo@vbalex.com](mailto:yo@vbalex.com) </sub>
*Estudiante de ciencias de la computación e informática.*  
*Estudiante de ingeniería eléctrica (electrónica y telecomunicaciones).*  
*Manitas de toda la vida, Manitas? Maker o Hacker actualmente.*  

### Descripción general

Nacido en el 92, nativo de Pérez Zeledon, San José, Costa Rica, actualmente vive en San Diego, Cartago. Desde pequeño ha buscado el como, el cuando y el por qué, de todo aquello que ha llegado a sus manos, en busca de re-interpretaciones y nuevas formas de uso, tratando de darle nueva vida a objetos que por obsolescencia programada o cosas de la vida dejaron de funcionar, con el tiempo se vio envuelto el la cultura maker donde ha elaborado/colaborado con proyectos de empresas privadas.


### Gustos

Amante del estar haciendo (adicto podría decirse).  
Desarrolla todo lo que pueda bajo licencias libres tanto para hardware como para software.  
Amante de las luces y los posibles usos de las mismas tanto a nivel funcional como artístico.  
Su fuerte es el software pero no se queda atrás en electrónica  y desarrollo de circuitos impresos.  


### Vida laboral

Se ha envuelto en proyectos de recuperación de material electrónico como [La Jauria] (http://lajauria.net) donde ha realizado instalaciones en conjunto a Joan Villaperros, Sergio Sasso; proyectos de fabricación digital y ayuda social como [Inventoría](https://www.facebook.com/inventoria/).

Ha trabajado con emprendimientos en distintas areas, entre ellas [Fundacion Costa Rica para la Innovación](http://funcostarica.og) que ha generado impacto en temas de educacion y enprendimiento sobre la población, [RQL Ingeniería](http://rqlingenieria.com) que brinda soluciones técnicas para el efectivo aprovechamiento de recursos (energía eléctrica y el agua), [Curiosidad Colectiva](http://curiosidadcolectiva.com) que trabaja sobre el estudio y comprencion del comportamiento humano aplicado al consumo y BTL.


### Hobbies

Suele estar trabajando en al menos un proyecto personal para compartir con sus amistades y el mundo entero (en [Proyectos](/proyectos) y [Tutoriales](/tutoriales) puedes encontrar algunos).
[Leer mas? Mira mi resumen](docs/personales/CV-AlexVargasBenamburg.pdf) 

Creado con [<i class="fas fa-heart"></i>](https://github.com/matcornic/hugo-theme-learn) en [Hugo](http://gohugo.io/)
