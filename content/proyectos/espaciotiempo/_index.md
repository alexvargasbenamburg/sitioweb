---
title: "Espacio Tiempo"
date: 2018-09-04T12:20:39-06:00 
draft: false
weight: 121
---

|                                                                            |                                                                       |
| :------------------------------------------------------------------------: | :-------------------------------------------------------------------: |
| ![EspacioTiempo](/imagenes/proyectos/espaciotiempo/portada.jpg?width=40pc) | ![EspacioTiempo](/imagenes/proyectos/espaciotiempo/uc.jpg?width=40pc) |

Para el 11,12 y 13 de noviembre del 2016 se presento proyecto ESPACIO TIEMPO "programa que <i>Iniciativas Interdisciplinarias</i> del Centro de Investigación, Docencia y Extensión Artística de la Universidad Nacional (Cidea-UNA) desarrolló, en conjunto con el Departamento de Física de la Facultad de Ciencias Exactas y Naturales y la Vicerrectoría de Investigación de la Universidad Estatal a Distancia, una propuesta en la que se visualiza la ciencia desde un lente artístico y tecnológico."

En este proyecto trabaje junto a grandes maestros en el area del Arte como [Joan Villaperros](https://www.facebook.com/joan.villaperros), y [Jose Chavarria](https://www.facebook.com/J.CHAVA), donde les colabore con un sistema de comunicacion inhalambrico para intercomunicar sus proyectos.


### Enlaces relacionados
[Nota de la NACION](https://www.nacion.com/viva/cultura/espacio-tiempo-lo-lleva-a-un-viaje-por-la-ciencia-a-traves-del-arte/CBSVCY4O5BGBNBNM7JIF5PMAYI/story/)  
[Artículo de CIDEA](http://www.cidea.una.ac.cr/index.php?option=com_content&view=article&id=103&Itemid=565)