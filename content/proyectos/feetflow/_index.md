---
title: "FeetFlow"
date: 2018-01-22T23:41:39-06:00
draft: false
weight: 101
tags: [moda,sonido,luz,arduino]
---

Para noviembre 2016, *Oliver Skinner* (Owner-Operator de Plivertees) tendría presencia en un programa televisivo de Teletica; Dancing with the Stars, como acompañante de Leonora Jiménez en conjunto a otras personas para presentar una coreografiá.
Tuvo la idea de decorar [pimpear] sus tenis, a Oliver le encantan la luces, en conjunto se decidió agregar luces que reaccionaran al sonido en la suela del zapato. Oliver se encargo de pegar las luces y pintar sus tenis; por otro lado yo me encargue conectar los electrónicos, adaptar una biblioteca de [FFT](https://github.com/alexvargasbenamburg/arduino_FFT), y crear un código que haciendo uso de esta modificara el brillo de las luces.
El día 20 de noviembre 2016 se estrenaron durante el show en vivo.
### Video
#### En acción
{{< youtube vbJHe_33b0E >}}
#### Proceso de preparación coreografía
{{< youtube uLB8GXwUpGM >}}

### Fotografias
|                                                       |                                                     |
| :---------------------------------------------------: | :-------------------------------------------------: |
|  ![Making 0](/imagenes/proyectos/feetflow/make0.png)  | ![Making 1](/imagenes/proyectos/feetflow/make1.png) |
| ![In the show](/imagenes/proyectos/feetflow/show.png) |   ![Pose](/imagenes/proyectos/feetflow/tenni.png)   |
<br/><br/>
### Enlaces relacionados
#### Oliver Skiner
-[Facebook](https://www.facebook.com/pliver)
-[Instagram](https://www.instagram.com/mr_pliver/)
#### Otros
  -[Tienda Plivertees](https://www.plivertees.com/)
  -[Repositorio del codigo](https://github.com/alexvargasbenamburg/arduino_FFT)

  <p align="right">*Fotografiás tomadas del perfil en Facebook de Oliver </p>
