---
title: "Lamparas"
date: 2018-01-22T21:02:35-06:00
draft: false
weight: 111
tags: [luz,corte laser,impresion 3d]
---
Normalmente no me gusta realizar regalias, aun asi, exiten personas que me han apoyado de multiples maneras en ditintos ambitos, como muestra de agradecimiento creo lamparas unicas con tematicas que le encanten a estas personas.
Aunque bueno tambien creo algunas para lucrar :P
*Por que lamparas?* Porque me encanta la luz.
*Por que no hacer otro igual?* Por la importancia que le tengo al vinculo con la persona.

Si te intereza adquirir alguna lampara de este estilo puedes escribirme.

### Fotografias
![Lamparas](/imagenes/proyectos/lamparas/lamps.png)

|                                           |                                           |
| :---------------------------------------: | :---------------------------------------: |
| {{< instagram BU-rlrvA6N1 hidecaption >}} | {{< instagram BVWD2ninnz4 hidecaption >}} |
| {{< instagram BWRUu48HBac hidecaption >}} | {{< instagram BWlpdihnHWa hidecaption >}} |
| {{< instagram BYRw0PGHxpD hidecaption >}} | {{< instagram BdBzyFoFe4x hidecaption >}} |
