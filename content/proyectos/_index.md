---
title: "Proyectos"
description: "Proyectos "
date: 2018-01-22T19:09:55-06:00
draft: false
weight: 100
---

Algunos proyectos que he trabajado ya sea solo, en alguna agrupacion, o pequenos aportes en el proyecto de alguien mas.

|                                                                                                   |                                                                           |
| :-----------------------------------------------------------------------------------------------: | :-----------------------------------------------------------------------: |
|             [![FeetFlow](/imagenes/proyectos/feetflow/logo.png) FeetFlow](./feetflow)             | [![Lamparas](/imagenes/proyectos/lamparas/logo.png) Lamparas](./lamparas) |
| [![EspacioTiempo](/imagenes/proyectos/espaciotiempo/portada.jpg) Espacio Tiempo](./espaciotiempo) |                             [CTRL-Z](./ctrlz)                             |
|                                      [Enjambre](./enjambre)                                       |                          [CEDULA CR](./cedulacr)                          |